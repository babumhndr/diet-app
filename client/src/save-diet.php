<?php include 'head.php';?>

		<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3" style="text-align: center;">
                            
                            <div class="card">
                                <div class="card-header" data-background-color="blue">
                                    <h4 class="title">Timings</h4>
                                    <p class="category">Save Diet</p>
                                </div>
                                <div class="card-content">
                                    <form id="save-diet-timings" method="post">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group label-floating">
                                                    <label class="">Breakfast : </label>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group label-floating">
                                                    <input type="time" name="breakfast" id="breakfast" placeholder="select Time" class="form-control" value="09:00">
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group label-floating">
                                                    <label class="">Lunch : </label>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group label-floating">
                                                    <input type="time" name="lunch" id="lunch" placeholder="select Time" class="form-control" value="13:00">
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group label-floating">
                                                    <label class="">Snacks : </label>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group label-floating">
                                                    <input type="time" name="snacks" id="snacks" placeholder="select Time" class="form-control" value="17:00">
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group label-floating">
                                                    <label class="">Dinner : </label>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group label-floating">
                                                    <input type="time" name="dinner" id="dinner" placeholder="select Time" class="form-control" value="20:00">
                                                </div>
                                            </div>
                                            
                                        </div>

                                        <button type="submit" class="btn btn-info" >Submit</button>
                                        <div class="clearfix"></div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


       <?php include 'foot.php';?>

       <script type="text/javascript" src="api/save-diet.js"></script>