<?php include 'head.php';?>

		<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3" style="text-align: center;">
                            
                            <div class="card">
                                <div class="card-header" data-background-color="blue">
                                    <h4 class="title">BMI Calculator</h4>
                                    <p class="category">Suggest New Diet</p>
                                </div>
                                <div class="card-content">
                                    <form id="caluculate_BMI" method="post">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group label-floating">
                                                    <label class="">Weight (in Kgs)</label>
                                                    <input type="number" name="weight" id="weight" placeholder="Enter Your Weight" class="form-control" required step="0.01">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group label-floating">
                                                    <label class="">Height (in Inches)</label>
                                                    <input type="number" name="height" id="height" placeholder="Enter Your Height" class="form-control" required step="0.01">
                                                </div>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-info">Calculate</button>
                                        <div class="clearfix"></div>
                                    </form>

                                    <hr>

                                    <h5>Your BMI is : <span class="text-info" id="bmi_value"></span></h5>
                                    <h5>You are : <span class="text-warning" id="result">...</span></h5>
                                    <a href="#" id="saveBMI"><p class="btn btn-danger">Get Diet</p></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


       <?php include 'foot.php';?>

       <script type="text/javascript" src="api/suggest-new-diet.js"> </script>