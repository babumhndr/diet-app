<?php include 'head.php';?>

		<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 " style="text-align: center;">
                            
                            <div class="card">
                                <a href="#" onclick="set_diet('veg')">
                                    <div class="card-header" data-background-color="blue">
                                        <h4 class="title">Veg Diet</h4>
                                        <small>( Click to choose diet )</small>
                                    </div>
                                </a>
                                <div class="card-content">
                                
                                    <table class="table">
                                        <tr>
                                            <th colspan="5" style="text-align: center;">
                                                Breakfast @ <script>document.write(localStorage.getItem('breakfast'));</script> Hrs
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>Item</td>
                                            <td>Protiens</td>
                                            <td>Carbs</td>
                                            <td>Calories</td>
                                            <td>Fat</td>
                                        </tr>
                                        <tr>
                                            <td id="bf_item"></td>
                                            <td id="bf_Protein"></td>
                                            <td id="bf_Carbs"></td>
                                            <td id="bf_Calories"></td>
                                            <td id="bf_Fat"></td>
                                        </tr>
                                    </table>

                                    <table class="table">
                                        <tr>
                                            <th colspan="5" style="text-align: center;">Lunch @ <script>document.write(localStorage.getItem('lunch'));</script> Hrs</th>
                                        </tr>
                                        <tr>
                                            <td>Item</td>
                                            <td>Protiens</td>
                                            <td>Carbs</td>
                                            <td>Calories</td>
                                            <td>Fat</td>
                                        </tr>
                                        <tr>
                                            <td id="l_item"></td>
                                            <td id="l_Protein"></td>
                                            <td id="l_Carbs"></td>
                                            <td id="l_Calories"></td>
                                            <td id="l_Fat"></td>
                                        </tr>
                                    </table>

                                    <table class="table">
                                        <tr>
                                            <th colspan="5" style="text-align: center;">Snacks @ <script>document.write(localStorage.getItem('snacks'));</script> Hrs</th>
                                        </tr>
                                        <tr>
                                            <td>Item</td>
                                            <td>Protiens</td>
                                            <td>Carbs</td>
                                            <td>Calories</td>
                                            <td>Fat</td>
                                        </tr>
                                        <tr>
                                            <td id="s_item"></td>
                                            <td id="s_Protein"></td>
                                            <td id="s_Carbs"></td>
                                            <td id="s_Calories"></td>
                                            <td id="s_Fat"></td>
                                        </tr>
                                    </table>
                                    <table class="table">
                                        <tr>
                                            <th colspan="5" style="text-align: center;">
                                                Breakfast @ <script>document.write(localStorage.getItem('dinner'));</script> Hrs
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>Item</td>
                                            <td>Protiens</td>
                                            <td>Carbs</td>
                                            <td>Calories</td>
                                            <td>Fat</td>
                                        </tr>
                                        <tr>
                                            <td id="d_item"></td>
                                            <td id="d_Protein"></td>
                                            <td id="d_Carbs"></td>
                                            <td id="d_Calories"></td>
                                            <td id="d_Fat"></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 " style="text-align: center;">
                            
                            <div class="card">
                              
                                <a href="#" onclick="set_diet('nonveg')">
                                    <div class="card-header" data-background-color="blue">
                                        <h4 class="title">Non-Veg Diet</h4>
                                        <small>( Click to choose diet )</small>
                                    </div>
                                </a>
                                <div class="card-content">
                                    <table class="table">
                                        <tr>
                                            <th colspan="5" style="text-align: center;">
                                                Breakfast @ <script>document.write(localStorage.getItem('breakfast'));</script> Hrs
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>Item</td>
                                            <td>Protiens</td>
                                            <td>Carbs</td>
                                            <td>Calories</td>
                                            <td>Fat</td>
                                        </tr>
                                        <tr>
                                            <td id="nbf_item"></td>
                                            <td id="nbf_Protein"></td>
                                            <td id="nbf_Carbs"></td>
                                            <td id="nbf_Calories"></td>
                                            <td id="nbf_Fat"></td>
                                        </tr>
                                    </table>

                                    <table class="table">
                                        <tr>
                                            <th colspan="5" style="text-align: center;">Lunch @ <script>document.write(localStorage.getItem('lunch'));</script> Hrs</th>
                                        </tr>
                                        <tr>
                                            <td>Item</td>
                                            <td>Protiens</td>
                                            <td>Carbs</td>
                                            <td>Calories</td>
                                            <td>Fat</td>
                                        </tr>
                                        <tr>
                                            <td id="nl_item"></td>
                                            <td id="nl_Protein"></td>
                                            <td id="nl_Carbs"></td>
                                            <td id="nl_Calories"></td>
                                            <td id="nl_Fat"></td>
                                        </tr>
                                    </table>

                                    <table class="table">
                                        <tr>
                                            <th colspan="5" style="text-align: center;">Snacks @ <script>document.write(localStorage.getItem('snacks'));</script> Hrs</th>
                                        </tr>
                                        <tr>
                                            <td>Item</td>
                                            <td>Protiens</td>
                                            <td>Carbs</td>
                                            <td>Calories</td>
                                            <td>Fat</td>
                                        </tr>
                                        <tr>
                                            <td id="ns_item"></td>
                                            <td id="ns_Protein"></td>
                                            <td id="ns_Carbs"></td>
                                            <td id="ns_Calories"></td>
                                            <td id="ns_Fat"></td>
                                        </tr>
                                    </table>

                                    <table class="table">
                                        
                                        <tr>
                                            <th colspan="5" style="text-align: center;">
                                                Breakfast @ <script>document.write(localStorage.getItem('dinner'));</script> Hrs
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>Item</td>
                                            <td>Protiens</td>
                                            <td>Carbs</td>
                                            <td>Calories</td>
                                            <td>Fat</td>
                                        </tr>
                                        <tr>
                                            <td id="nd_item"></td>
                                            <td id="nd_Protein"></td>
                                            <td id="nd_Carbs"></td>
                                            <td id="nd_Calories"></td>
                                            <td id="nd_Fat"></td>
                                        </tr>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


       <?php include 'foot.php';?>


       <script type="text/javascript" src="api/choose-diet.js"></script>