<?php include 'head.php';?>

		<div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                          
                                <p><b>My BMI : </b><span id="bmi"></span></p>
                                <p><b>Last BMI Calculated On : </b><span id="bmi_updated"></span></p>
                                <p><b>My Height : </b><span id="height"></span> Inches</p>
                                <p><b>My Weight : </b><span id="weight"></span> Kgs</p>
                          
                        </div>
                        <div class="col-md-6">
                                <p><b>Breakfast : </b><span id="breakfast"></span> Hrs</p>
                                <p><b>Lunch : </b><span id="lunch"></span> Hrs</p>
                                <p><b>Snacks : </b><span id="snacks"></span> Hrs</p>
                                <p><b>Dinner : </b><span id="dinner"></span> Hrs</p>
                        </div>
                        <div class="col-md-12">
                            <h4>Result : <span id="result"></span></h4>
                        </div>
                    </div>
                </div>
            </div>


       <?php include 'foot.php';?>

       <script type="text/javascript" src="api/current-diet.js"></script>