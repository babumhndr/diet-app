
           $.getJSON('../food_items.json').done(function(items){
                 console.log(items);
                 filterItems(items);

            });
                 var veg_B = [];
                 var veg_L = [];
                 var veg_S = [];
                 var veg_D = [];
                 var nveg_B = [];
                 var nveg_L = [];
                 var nveg_S = [];
                 var nveg_D = [];
           function filterItems(items){
            
                 
                 var bmi = localStorage.getItem('bmi');
                 var result;
                 if (bmi <= 18.5) {
                    result = 'UNDER WEIGHT';
                    }else if (bmi > 18.5 && bmi <= 29.9) {
                        result = "NORMAL";
                    }if (bmi >= 30 ) {
                        result = 'OVER WEIGHT';
                    }

                    console.log(result);
                 for (var i = 0; i < items.data.length; i++) {
                     if (items.data[i].Veg=='Yes' && items.data[i].Category=='B' && items.data[i].foodType==result) {
                        veg_B.push(items.data[i]);
                     }

                     if (items.data[i].Veg=='Yes' && items.data[i].Category=='L' && items.data[i].foodType==result) {
                        veg_L.push(items.data[i]);
                     }

                     if (items.data[i].Veg=='Yes' && items.data[i].Category=='L' && items.data[i].foodType==result) {
                        veg_S.push(items.data[i]);
                     }

                     if (items.data[i].Veg=='Yes' && items.data[i].Category=='D' && items.data[i].foodType==result) {
                        veg_D.push(items.data[i]);
                     }

                     if (items.data[i].Veg=='No' && items.data[i].Category=='B' && items.data[i].foodType==result) {
                        nveg_B.push(items.data[i]);
                     }

                     if (items.data[i].Veg=='No' && items.data[i].Category=='L' && items.data[i].foodType==result) {
                        nveg_L.push(items.data[i]);
                     }

                     if (items.data[i].Veg=='No' && items.data[i].Category=='S' && items.data[i].foodType==result) {
                        nveg_S.push(items.data[i]);
                     }

                     if (items.data[i].Veg=='No' && items.data[i].Category=='D' && items.data[i].foodType==result) {
                        nveg_D.push(items.data[i]);
                     }
                 }
                 console.log(veg_B);
                 console.log(veg_L);
                 console.log(veg_S);
                 console.log(veg_D);
                 console.log(nveg_B);
                 console.log(nveg_L);
                 console.log(nveg_S);
                 console.log(nveg_D);

                 assignFood();
                 

                 
           }

           function assignFood(){
                v_breakfast(veg_B, getRandomInt(0, veg_B.length-1));
                 v_lunch(veg_L, getRandomInt(0, veg_L.length-1));
                 v_snacks(veg_S, getRandomInt(0, veg_S.length-1));
                 v_dinner(veg_D, getRandomInt(0, veg_D.length-1));
                 nv_breakfast(nveg_B, getRandomInt(0, nveg_B.length-1));
                 nv_lunch(nveg_L, getRandomInt(0, nveg_L.length-1));
                 nv_snacks(nveg_S, getRandomInt(0, nveg_S.length-1));
                 nv_dinner(nveg_D, getRandomInt(0, nveg_D.length-1));

           }

           function getRandomInt(min, max) {
                return Math.floor(Math.random() * (max - min + 1)) + min;
            }

            function v_breakfast(list, index){
                localStorage.setItem('vbf_food_id', list[index].food_id);
                $('#bf_item').html(list[index].Items);
                $('#bf_food_id').html(list[index].food_id);
                $('#bf_Protein').html(list[index].Protein);
                $('#bf_Carbs').html(list[index].Carbs);
                $('#bf_Calories').html(list[index].Calories);
                $('#bf_Fat').html(list[index].Fat);
                return;
            }
            function v_lunch(list, index){
                localStorage.setItem('vl_food_id', list[index].food_id);
                $('#l_item').html(list[index].Items);
                $('#l_food_id').html(list[index].food_id);
                $('#l_Protein').html(list[index].Protein);
                $('#l_Carbs').html(list[index].Carbs);
                $('#l_Calories').html(list[index].Calories);
                $('#l_Fat').html(list[index].Fat);
                return;
            }
            function v_snacks(list, index){
                localStorage.setItem('vs_food_id', list[index].food_id);
                $('#s_item').html(list[index].Items);
                $('#s_food_id').html(list[index].food_id);
                $('#s_Protein').html(list[index].Protein);
                $('#s_Carbs').html(list[index].Carbs);
                $('#s_Calories').html(list[index].Calories);
                $('#s_Fat').html(list[index].Fat);
                return;
            }
            function v_dinner(list, index){
                localStorage.setItem('vd_food_id', list[index].food_id);
                $('#d_item').html(list[index].Items);
                $('#d_food_id').html(list[index].food_id);
                $('#d_Protein').html(list[index].Protein);
                $('#d_Carbs').html(list[index].Carbs);
                $('#d_Calories').html(list[index].Calories);
                $('#d_Fat').html(list[index].Fat);
                return;
            }
            function nv_breakfast(list, index){
                localStorage.setItem('nbf_food_id', list[index].food_id);
                $('#nbf_item').html(list[index].Items);
                $('#nbf_food_id').html(list[index].food_id);
                $('#nbf_Protein').html(list[index].Protein);
                $('#nbf_Carbs').html(list[index].Carbs);
                $('#nbf_Calories').html(list[index].Calories);
                $('#nbf_Fat').html(list[index].Fat);
                return;
            }
            function nv_lunch(list, index){
                localStorage.setItem('nl_food_id', list[index].food_id);
                $('#nl_item').html(list[index].Items);
                $('#nl_food_id').html(list[index].food_id);
                $('#nl_Protein').html(list[index].Protein);
                $('#nl_Carbs').html(list[index].Carbs);
                $('#nl_Calories').html(list[index].Calories);
                $('#nl_Fat').html(list[index].Fat);
                return;
            }
            function nv_snacks(list, index){
                localStorage.setItem('ns_food_id', list[index].food_id);
                $('#ns_item').html(list[index].Items);
                $('#ns_food_id').html(list[index].food_id);
                $('#ns_Protein').html(list[index].Protein);
                $('#ns_Carbs').html(list[index].Carbs);
                $('#ns_Calories').html(list[index].Calories);
                $('#ns_Fat').html(list[index].Fat);
                return;
            }
            function nv_dinner(list, index){
                localStorage.setItem('nd_food_id', list[index].food_id);
                $('#nd_item').html(list[index].Items);
                $('#nd_food_id').html(list[index].food_id);
                $('#nd_Protein').html(list[index].Protein);
                $('#nd_Carbs').html(list[index].Carbs);
                $('#nd_Calories').html(list[index].Calories);
                $('#nd_Fat').html(list[index].Fat);
                return;
            }


            function set_diet(type){
                demo.showNotification('bottom','center','Your diet plan will be saved.');
                var bf,l,s,d;
                if (type=='veg') {
                    bf = localStorage.getItem('vbf_food_id');
                    l = localStorage.getItem('vl_food_id');
                    s = localStorage.getItem('vs_food_id');
                    d = localStorage.getItem('vd_food_id');
                }else{
                    bf = localStorage.getItem('nbf_food_id');
                    l = localStorage.getItem('nl_food_id');
                    s = localStorage.getItem('ns_food_id');
                    d = localStorage.getItem('nd_food_id');
                }
                $.ajax({
                        type: 'POST',
                        url: "http://18.216.229.139/diet-app/server/bmi/set-diet-plan.php",
                        data:{
                            'bf':bf,
                            'l':l,
                            's':s,
                            'd':d,
                            'type':type,
                            'user_id': localStorage.getItem('id')
                        },
                        success: function (data) {
                            console.log(data);
                            var result = JSON.parse(data);
                            if(result.status=="failure"){
                                  demo.showNotification('bottom','center',result.message);
                                }else if(result.status=="success"){
                                  demo.showNotification('bottom','center',result.message);
                                  window.location.href = "index";
                                }
                            
                        },
                        error: function (data) {
                            console.log('An error occurred.');
                            console.log(data);
                            demo.showNotification('bottom','center',data);
                        }
                    });
            }
     