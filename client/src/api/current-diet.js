$(document).ready(function() {
    			
    			$.ajax({
			            type: 'POST',
			            url: "http://18.216.229.139/diet-app/server/bmi/current-diet.php",
			            data:{ 'user_id': localStorage.getItem('id')},
			            success: function (data) {
			            	console.log(data);
			                var result = JSON.parse(data);
			                if(result.status=="failure"){
			                      demo.showNotification('bottom','center',result.message);
			                    }else if(result.status=="success"){
			                      console.log(result);
			                      $('#bmi').html(result.data["0"].bmi);
			                      $('#bmi_updated').html(result.data["0"].bmi_updated);
			                      $('#height').html(result.data["0"].height);
			                      $('#weight').html(result.data["0"].weight);

			                      $('#breakfast').html(result.data["0"].breakfast);
			                      $('#lunch').html(result.data["0"].lunch);
			                      $('#snacks').html(result.data["0"].snacks);
			                      $('#dinner').html(result.data["0"].dinner);

			                      $('#result').html(result.data["0"].result);
			                    }
			                
			            },
			            error: function (data) {
			                console.log('An error occurred.');
			                console.log(data);
			                demo.showNotification('bottom','center',data);
			            }
			        });	
  });

