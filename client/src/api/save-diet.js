$(document).ready(function() {
    			var frm = $('#save-diet-timings');

   			    frm.submit(function (e) {

   			    	e.preventDefault();

   			    localStorage.setItem('breakfast', $('#breakfast').val());
   			    localStorage.setItem('lunch', $('#lunch').val());
   			    localStorage.setItem('snacks', $('#snacks').val());
   			    localStorage.setItem('dinner', $('#dinner').val());

    			$.ajax({
			            type: 'POST',
			            url: "http://18.216.229.139/diet-app/server/bmi/save-diet.php",
			            data:frm.serialize()+'&'+$.param({ 'user_id': localStorage.getItem('id')}),
			            success: function (data) {
			            	console.log(data);
			                var result = JSON.parse(data);
			                if(result.status=="failure"){
			                      demo.showNotification('bottom','center',result.message);
			                    }else if(result.status=="success"){
			                      demo.showNotification('bottom','center',result.message);
			                      window.location.href = "choose-diet";
			                    }
			                
			            },
			            error: function (data) {
			                console.log('An error occurred.');
			                console.log(data);
			                demo.showNotification('bottom','center',data);
			            }
			        });
   				 });	
  });

