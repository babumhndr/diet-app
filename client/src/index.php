<?php include 'head.php';?>

             <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <h3 style="text-align: center;">You have eaten <span class="text-danger">396 calories</span>  average per day , Gained  <span class="text-warning">10 lbs</span></h3>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header card-chart" data-background-color="green">
                                    <div class="ct-chart" id="dailySalesChart"><svg xmlns:ct="http://gionkunz.github.com/chartist-js/ct" width="100%" height="100%" class="ct-chart-line" style="width: 100%; height: 100%;">
                                        <g class="ct-grids">
                                            <line x1="40" x2="40" y1="0" y2="120" class="ct-grid ct-horizontal"></line>
                                            <line x1="88.71428571428572" x2="88.71428571428572" y1="0" y2="120" class="ct-grid ct-horizontal"></line>
                                            <line x1="137.42857142857144" x2="137.42857142857144" y1="0" y2="120" class="ct-grid ct-horizontal"></line>
                                            <line x1="186.14285714285714" x2="186.14285714285714" y1="0" y2="120" class="ct-grid ct-horizontal"></line>
                                            <line x1="234.85714285714286" x2="234.85714285714286" y1="0" y2="120" class="ct-grid ct-horizontal"></line>
                                            <line x1="283.57142857142856" x2="283.57142857142856" y1="0" y2="120" class="ct-grid ct-horizontal"></line>
                                            <line x1="332.2857142857143" x2="332.2857142857143" y1="0" y2="120" class="ct-grid ct-horizontal"></line>
                                            <line y1="120" y2="120" x1="40" x2="381" class="ct-grid ct-vertical"></line>
                                            <line y1="96" y2="96" x1="40" x2="381" class="ct-grid ct-vertical"></line>
                                            <line y1="72" y2="72" x1="40" x2="381" class="ct-grid ct-vertical"></line>
                                            <line y1="48" y2="48" x1="40" x2="381" class="ct-grid ct-vertical"></line>
                                            <line y1="24" y2="24" x1="40" x2="381" class="ct-grid ct-vertical"></line>
                                            <line y1="0" y2="0" x1="40" x2="381" class="ct-grid ct-vertical"></line>
                                        </g>
                                        <g>
                                            <g class="ct-series ct-series-a">
                                                <path d="M 40 91.2 C 88.714 79.2 88.714 79.2 88.714 79.2 C 137.429 103.2 137.429 103.2 137.429 103.2 C 186.143 79.2 186.143 79.2 186.143 79.2 C 234.857 64.8 234.857 64.8 234.857 64.8 C 283.571 76.8 283.571 76.8 283.571 76.8 C 332.286 28.8 332.286 28.8 332.286 28.8" class="ct-line"></path>
                                                <line x1="40" y1="91.2" x2="40.01" y2="91.2" class="ct-point" ct:value="12" opacity="1"></line>
                                                <line x1="88.71428571428572" y1="79.2" x2="88.72428571428573" y2="79.2" class="ct-point" ct:value="17" opacity="1"></line>
                                                <line x1="137.42857142857144" y1="103.2" x2="137.43857142857144" y2="103.2" class="ct-point" ct:value="7" opacity="1"></line>
                                                <line x1="186.14285714285714" y1="79.2" x2="186.15285714285713" y2="79.2" class="ct-point" ct:value="17" opacity="1"></line>
                                                <line x1="234.85714285714286" y1="64.8" x2="234.86714285714285" y2="64.8" class="ct-point" ct:value="23" opacity="1"></line>
                                                <line x1="283.57142857142856" y1="76.8" x2="283.58142857142855" y2="76.8" class="ct-point" ct:value="18" opacity="1"></line>
                                                <line x1="332.2857142857143" y1="28.799999999999997" x2="332.29571428571427" y2="28.799999999999997" class="ct-point" ct:value="38" opacity="1"></line>
                                            </g>
                                        </g>
                                        <g class="ct-labels">
                                            <foreignObject style="overflow: visible;" x="40" y="125" width="48.714285714285715" height="20">
                                                <span class="ct-label ct-horizontal ct-end" style="width: 49px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">05 Feb</span>
                                            </foreignObject>
                                            <foreignObject style="overflow: visible;" x="88.71428571428572" y="125" width="48.714285714285715" height="20">
                                                <span class="ct-label ct-horizontal ct-end" style="width: 49px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">06 Feb</span>
                                            </foreignObject>
                                            <foreignObject style="overflow: visible;" x="137.42857142857144" y="125" width="48.71428571428571" height="20">
                                                <span class="ct-label ct-horizontal ct-end" style="width: 49px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">07 Feb</span>
                                            </foreignObject>
                                            <foreignObject style="overflow: visible;" x="186.14285714285714" y="125" width="48.71428571428572" height="20"><span class="ct-label ct-horizontal ct-end" style="width: 49px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">08 Feb</span></foreignObject>
                                            <foreignObject style="overflow: visible;" x="234.85714285714286" y="125" width="48.71428571428572" height="20"><span class="ct-label ct-horizontal ct-end" style="width: 49px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">09 Feb</span></foreignObject>
                                            <foreignObject style="overflow: visible;" x="283.57142857142856" y="125" width="48.714285714285694" height="20"><span class="ct-label ct-horizontal ct-end" style="width: 49px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">10 Feb</span></foreignObject>
                                            <foreignObject style="overflow: visible;" x="332.2857142857143" y="125" width="48.71428571428572" height="20"><span class="ct-label ct-horizontal ct-end" style="width: 49px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">11 Feb</span></foreignObject>
                                            <foreignObject style="overflow: visible;" y="96" x="0" height="24" width="30"><span class="ct-label ct-vertical ct-start" style="height: 24px; width: 30px" xmlns="http://www.w3.org/1999/xhtml">0</span></foreignObject><foreignObject style="overflow: visible;" y="72" x="0" height="24" width="30"><span class="ct-label ct-vertical ct-start" style="height: 24px; width: 30px" xmlns="http://www.w3.org/1999/xhtml">10</span></foreignObject><foreignObject style="overflow: visible;" y="48" x="0" height="24" width="30"><span class="ct-label ct-vertical ct-start" style="height: 24px; width: 30px" xmlns="http://www.w3.org/1999/xhtml">20</span></foreignObject><foreignObject style="overflow: visible;" y="24" x="0" height="24" width="30"><span class="ct-label ct-vertical ct-start" style="height: 24px; width: 30px" xmlns="http://www.w3.org/1999/xhtml">30</span></foreignObject><foreignObject style="overflow: visible;" y="0" x="0" height="24" width="30"><span class="ct-label ct-vertical ct-start" style="height: 24px; width: 30px" xmlns="http://www.w3.org/1999/xhtml">40</span></foreignObject><foreignObject style="overflow: visible;" y="-30" x="0" height="30" width="30"><span class="ct-label ct-vertical ct-start" style="height: 30px; width: 30px" xmlns="http://www.w3.org/1999/xhtml">50</span></foreignObject></g></svg></div>
                                </div>
                                <div class="card-content">
                                    <h4 class="title">Calories</h4>
                                    
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="material-icons">chevron_right</i>
                                        <a href="agent-list">My Current Diet</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header card-chart" data-background-color="red">
                                    <div class="ct-chart" id="dailySalesChart"><svg xmlns:ct="http://gionkunz.github.com/chartist-js/ct" width="100%" height="100%" class="ct-chart-line" style="width: 100%; height: 100%;">
                                        <g class="ct-grids">
                                            <line x1="40" x2="40" y1="0" y2="180" class="ct-grid ct-horizontal"></line>
                                            <line x1="88.71428571428572" x2="88.71428571428572" y1="0" y2="120" class="ct-grid ct-horizontal"></line>
                                            <line x1="137.42857142857144" x2="137.42857142857144" y1="0" y2="120" class="ct-grid ct-horizontal"></line>
                                            <line x1="186.14285714285714" x2="186.14285714285714" y1="0" y2="120" class="ct-grid ct-horizontal"></line>
                                            <line x1="234.85714285714286" x2="234.85714285714286" y1="0" y2="120" class="ct-grid ct-horizontal"></line>
                                            <line x1="283.57142857142856" x2="283.57142857142856" y1="0" y2="120" class="ct-grid ct-horizontal"></line>
                                            <line x1="332.2857142857143" x2="332.2857142857143" y1="0" y2="120" class="ct-grid ct-horizontal"></line>
                                            <line y1="120" y2="120" x1="40" x2="381" class="ct-grid ct-vertical"></line>
                                            <line y1="96" y2="96" x1="40" x2="381" class="ct-grid ct-vertical"></line>
                                            <line y1="72" y2="72" x1="40" x2="381" class="ct-grid ct-vertical"></line>
                                            <line y1="48" y2="48" x1="40" x2="381" class="ct-grid ct-vertical"></line>
                                            <line y1="24" y2="24" x1="40" x2="381" class="ct-grid ct-vertical"></line>
                                            <line y1="0" y2="0" x1="40" x2="381" class="ct-grid ct-vertical"></line>
                                        </g>
                                        <g>
                                            <g class="ct-series ct-series-a">
                                                <path d="M 40 91.2 C 88.714 79.2 88.714 79.2 88.714 79.2 C 137.429 103.2 137.429 103.2 137.429 103.2 C 186.143 79.2 186.143 79.2 186.143 79.2 C 234.857 64.8 234.857 64.8 234.857 64.8 C 283.571 76.8 283.571 76.8 283.571 76.8 C 332.286 28.8 332.286 28.8 332.286 28.8" class="ct-line"></path>
                                                <line x1="40" y1="91.2" x2="40.01" y2="91.2" class="ct-point" ct:value="12" opacity="1"></line>
                                                <line x1="88.71428571428572" y1="79.2" x2="88.72428571428573" y2="79.2" class="ct-point" ct:value="17" opacity="1"></line>
                                                <line x1="137.42857142857144" y1="103.2" x2="137.43857142857144" y2="103.2" class="ct-point" ct:value="7" opacity="1"></line>
                                                <line x1="186.14285714285714" y1="79.2" x2="186.15285714285713" y2="79.2" class="ct-point" ct:value="17" opacity="1"></line>
                                                <line x1="234.85714285714286" y1="64.8" x2="234.86714285714285" y2="64.8" class="ct-point" ct:value="23" opacity="1"></line>
                                                <line x1="283.57142857142856" y1="76.8" x2="283.58142857142855" y2="76.8" class="ct-point" ct:value="18" opacity="1"></line>
                                                <line x1="332.2857142857143" y1="28.799999999999997" x2="332.29571428571427" y2="28.799999999999997" class="ct-point" ct:value="38" opacity="1"></line>
                                            </g>
                                        </g>
                                        <g class="ct-labels">
                                            <foreignObject style="overflow: visible;" x="40" y="125" width="48.714285714285715" height="20">
                                                <span class="ct-label ct-horizontal ct-end" style="width: 49px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">05 Feb</span>
                                            </foreignObject>
                                            <foreignObject style="overflow: visible;" x="88.71428571428572" y="125" width="48.714285714285715" height="20">
                                                <span class="ct-label ct-horizontal ct-end" style="width: 49px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">06 Feb</span>
                                            </foreignObject>
                                            <foreignObject style="overflow: visible;" x="137.42857142857144" y="125" width="48.71428571428571" height="20">
                                                <span class="ct-label ct-horizontal ct-end" style="width: 49px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">07 Feb</span>
                                            </foreignObject>
                                            <foreignObject style="overflow: visible;" x="186.14285714285714" y="125" width="48.71428571428572" height="20"><span class="ct-label ct-horizontal ct-end" style="width: 49px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">08 Feb</span></foreignObject>
                                            <foreignObject style="overflow: visible;" x="234.85714285714286" y="125" width="48.71428571428572" height="20"><span class="ct-label ct-horizontal ct-end" style="width: 49px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">09 Feb</span></foreignObject>
                                            <foreignObject style="overflow: visible;" x="283.57142857142856" y="125" width="48.714285714285694" height="20"><span class="ct-label ct-horizontal ct-end" style="width: 49px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">10 Feb</span></foreignObject>
                                            <foreignObject style="overflow: visible;" x="332.2857142857143" y="125" width="48.71428571428572" height="20"><span class="ct-label ct-horizontal ct-end" style="width: 49px; height: 20px" xmlns="http://www.w3.org/1999/xhtml">11 Feb</span></foreignObject>
                                            <foreignObject style="overflow: visible;" y="96" x="0" height="24" width="30"><span class="ct-label ct-vertical ct-start" style="height: 24px; width: 30px" xmlns="http://www.w3.org/1999/xhtml">0</span></foreignObject><foreignObject style="overflow: visible;" y="72" x="0" height="24" width="30"><span class="ct-label ct-vertical ct-start" style="height: 24px; width: 30px" xmlns="http://www.w3.org/1999/xhtml">20</span></foreignObject><foreignObject style="overflow: visible;" y="48" x="0" height="24" width="30"><span class="ct-label ct-vertical ct-start" style="height: 24px; width: 30px" xmlns="http://www.w3.org/1999/xhtml">40</span></foreignObject><foreignObject style="overflow: visible;" y="24" x="0" height="24" width="30"><span class="ct-label ct-vertical ct-start" style="height: 24px; width: 30px" xmlns="http://www.w3.org/1999/xhtml">60</span></foreignObject><foreignObject style="overflow: visible;" y="0" x="0" height="24" width="30"><span class="ct-label ct-vertical ct-start" style="height: 24px; width: 30px" xmlns="http://www.w3.org/1999/xhtml">80</span></foreignObject><foreignObject style="overflow: visible;" y="-30" x="0" height="30" width="30"><span class="ct-label ct-vertical ct-start" style="height: 30px; width: 30px" xmlns="http://www.w3.org/1999/xhtml">100</span></foreignObject></g></svg></div>
                                </div>
                                <div class="card-content">
                                    <h4 class="title">Weight</h4>
                                    
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="material-icons">chevron_right</i>
                                        <a href="agent-list">Suggest A Diet</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
             </div>
            </div>

<?php include 'foot.php';?>