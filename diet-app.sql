-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 19, 2018 at 03:23 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `diet-app`
--

-- --------------------------------------------------------

--
-- Table structure for table `bmi_score`
--

CREATE TABLE `bmi_score` (
  `bmi_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `height` float NOT NULL,
  `weight` float NOT NULL,
  `bmi` float NOT NULL,
  `result` varchar(256) NOT NULL,
  `bmi_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `bmi_updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bmi_score`
--

INSERT INTO `bmi_score` (`bmi_id`, `user_id`, `height`, `weight`, `bmi`, `result`, `bmi_created`, `bmi_updated`) VALUES
(1, 1, 2, 80, 28, 'Normal Weight', '2018-03-18 22:00:09', '2018-03-18 22:00:09'),
(2, 1, 2, 80, 32, 'Over Weight', '2018-03-18 22:04:19', '2018-03-18 22:04:19'),
(3, 2, 2, 80, 25, 'Normal Weight', '2018-03-19 00:47:13', '2018-03-19 00:47:13'),
(4, 2, 2, 82, 32, 'Over Weight', '2018-03-19 01:28:24', '2018-03-19 01:28:24'),
(5, 2, 2, 80, 32, 'Over Weight', '2018-03-19 01:30:24', '2018-03-19 01:30:24'),
(6, 2, 1.5748, 80, 32.2581, 'Over Weight', '2018-03-19 01:32:11', '2018-03-19 01:32:11'),
(7, 2, 1.6002, 81, 31.6327, 'Over Weight', '2018-03-19 01:33:52', '2018-03-19 01:33:52'),
(8, 2, 62, 80, 32.2581, 'Over Weight', '2018-03-19 01:36:48', '2018-03-19 01:36:48'),
(9, 2, 62.19, 82.5, 33.0632, 'Over Weight', '2018-03-19 01:39:04', '2018-03-19 01:39:04'),
(10, 2, 62, 80, 32.2581, 'Over Weight', '2018-03-19 16:02:34', '2018-03-19 16:02:34'),
(11, 1, 62.3, 84, 33.5456, 'Over Weight', '2018-03-19 18:29:13', '2018-03-19 18:29:13'),
(12, 1, 62.3, 80, 31.9482, 'Over Weight', '2018-03-19 18:34:23', '2018-03-19 18:34:23'),
(13, 1, 62.3, 80, 31.9482, 'Over Weight', '2018-03-19 18:38:44', '2018-03-19 18:38:44'),
(14, 2, 65.3, 80.2, 29.1528, 'Normal Weight', '2018-03-19 19:05:57', '2018-03-19 19:05:57');

-- --------------------------------------------------------

--
-- Table structure for table `food_items`
--

CREATE TABLE `food_items` (
  `food_id` int(11) NOT NULL,
  `Items` varchar(27) DEFAULT NULL,
  `Protein(g)` decimal(5,2) DEFAULT NULL,
  `Carbs(g)` decimal(5,2) DEFAULT NULL,
  `Calories` decimal(5,2) DEFAULT NULL,
  `Fat(g)` decimal(5,2) DEFAULT NULL,
  `Veg` varchar(3) DEFAULT NULL,
  `Category` varchar(1) DEFAULT NULL,
  `Food type` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `food_items`
--

INSERT INTO `food_items` (`food_id`, `Items`, `Protein(g)`, `Carbs(g)`, `Calories`, `Fat(g)`, `Veg`, `Category`, `Food type`) VALUES
(1, 'Chapati', '2.64', '13.01', '64.00', '0.66', 'Yes', 'B', 'OVER WEIGHT'),
(2, 'Puri', '3.00', '16.00', '107.00', '5.00', 'Yes', 'B', 'NORMAL'),
(3, 'Parota(alu)', '0.00', '0.00', '65.00', '0.00', 'Yes', 'B', 'OVER WEIGHT'),
(4, 'Parota(maida)', '21.00', '50.00', '790.00', '13.00', 'Yes', 'B', 'UNDER WEIGHT'),
(5, 'Parota(meti)', '5.00', '23.00', '134.00', '3.00', 'Yes', 'B', 'NORMAL'),
(6, 'Roti(ragi)', '6.00', '72.00', '320.00', '2.00', 'Yes', 'B', 'NORMAL'),
(7, 'Roti(jowar)', '1.00', '12.00', '75.00', '3.00', 'Yes', 'L', 'OVER WEIGHT'),
(8, 'Roti(rice floor)', '29.00', '14.00', '428.00', '28.00', 'Yes', 'L', 'NORMAL'),
(9, 'Dosa(rava)', '53.00', '90.00', '999.99', '82.00', 'Yes', 'B', 'UNDER WEIGHT'),
(10, 'Dosa(wheat)', '3.00', '94.00', '999.99', '125.00', 'Yes', 'B', 'UNDER WEIGHT'),
(11, 'Dosa(ragi)', '0.00', '0.00', '300.00', '0.00', 'Yes', 'B', 'NORMAL'),
(12, 'Dosa(rice floor)', '0.00', '0.00', '217.00', '5.00', 'Yes', 'B', 'NORMAL'),
(13, 'Idli', '36.00', '62.00', '516.00', '8.00', 'Yes', 'B', 'NORMAL'),
(14, 'Idli(rava)', '9.00', '44.00', '240.00', '4.00', 'Yes', 'B', 'NORMAL'),
(15, 'Idli(ragi)', '37.00', '411.00', '999.99', '144.00', 'Yes', 'B', 'UNDER WEIGHT'),
(16, 'Ragi Mudde(100g)', '153.00', '111.00', '999.99', '30.00', 'Yes', 'L', 'UNDER WEIGHT'),
(17, 'Bisibelebath(150gm)', '18.00', '5.00', '173.00', '9.00', 'Yes', 'D', 'NORMAL'),
(18, 'Semiya upma', '4.00', '17.00', '93.00', '2.00', 'Yes', 'D', 'OVER WEIGHT'),
(19, 'Upma(rava)', '15.00', '37.00', '321.00', '12.00', 'Yes', 'L', 'NORMAL'),
(20, 'Maggi(100g)', '7.00', '45.00', '306.00', '11.00', 'Yes', 'S', 'OVER WEIGHT'),
(21, 'Noodles(100g)', '4.00', '25.00', '137.00', '2.00', 'Yes', 'S', 'NORMAL'),
(22, 'Roti(tandoori)', '7.00', '20.00', '120.00', '1.00', 'Yes', 'D', 'NORMAL'),
(23, 'Roti(nan)', '0.00', '26.00', '164.00', '3.00', 'Yes', 'D', 'NORMAL'),
(24, 'Kulcha', '2.00', '5.00', '25.00', '0.00', 'Yes', 'D', 'OVER WEIGHT'),
(25, 'Rumali Roti', '18.00', '173.00', '880.00', '13.00', 'Yes', 'D', 'NORMAL'),
(26, 'Rice(100g)', '2.00', '29.00', '130.00', '0.00', 'Yes', 'L', 'UNDER WEIGHT'),
(27, 'Veg Pulao', '3.00', '19.00', '119.00', '3.40', 'Yes', 'L', 'NORMAL'),
(28, 'Veg Schezwan Fried Rice', '0.00', '22.00', '130.00', '4.00', 'Yes', 'S', 'UNDER WEIGHT'),
(29, 'Veg fried rice', '6.30', '21.06', '168.00', '6.23', 'Yes', 'L', 'OVER WEIGHT'),
(30, 'Veg Biriyani', '56.00', '430.00', '999.99', '135.00', 'Yes', 'L', 'UNDER WEIGHT'),
(31, 'Ghee rice', '5.00', '26.00', '170.00', '5.00', 'Yes', 'L', 'OVER WEIGHT'),
(32, 'Jeera rice', '23.00', '11.00', '175.00', '6.00', 'Yes', 'B', 'UNDER WEIGHT'),
(33, 'Lemon rice', '8.00', '13.00', '80.00', '0.00', 'Yes', 'B', 'OVER WEIGHT'),
(34, 'Coconut rice', '14.00', '53.00', '230.00', '4.00', 'Yes', 'B', 'NORMAL'),
(35, 'Vangibath', '3.00', '26.00', '160.00', '5.00', 'Yes', 'L', 'OVER WEIGHT'),
(36, 'Mushroom fried rice', '101.00', '7.00', '784.00', '31.00', 'Yes', 'L', 'NORMAL'),
(37, 'Pongal', '6.00', '8.00', '80.00', '4.00', 'Yes', 'B', 'OVER WEIGHT'),
(38, 'Kaju curry', '108.00', '186.00', '999.99', '150.00', 'Yes', 'D', 'UNDER WEIGHT'),
(39, 'Dal', '4.00', '13.00', '138.00', '7.00', 'Yes', 'D', 'NORMAL'),
(40, 'Panner butter masala', '9.00', '5.00', '229.00', '19.00', 'Yes', 'D', 'UNDER WEIGHT'),
(41, 'panner tikka masala', '8.00', '18.00', '180.00', '9.00', 'Yes', 'D', 'UNDER WEIGHT'),
(42, 'Sambar', '5.00', '16.00', '114.00', '4.00', 'Yes', 'L', 'NORMAL'),
(43, 'Channa masala', '6.00', '18.00', '170.00', '8.00', 'Yes', 'D', 'UNDER WEIGHT'),
(44, 'Brinjal curry', '2.00', '17.00', '135.00', '5.00', 'Yes', 'L', 'NORMAL'),
(45, 'Alu curry', '14.00', '16.00', '77.00', '2.00', 'Yes', 'L', 'OVER WEIGHT'),
(46, 'Tomato curry', '4.00', '22.00', '170.00', '6.00', 'Yes', 'L', 'NORMAL'),
(47, 'Peas curry', '16.00', '48.00', '420.00', '20.00', 'Yes', 'L', 'OVER WEIGHT'),
(48, 'Fish fry ', '8.80', '11.80', '160.00', '8.70', 'No', 'B', 'OVER WEIGHT'),
(49, 'Fish Nuggets', '14.00', '9.00', '240.00', '17.00', 'No', 'S', 'NORMAL'),
(50, 'Fish Malabar', '16.00', '7.00', '199.00', '12.00', 'No', 'L', 'OVER WEIGHT'),
(51, 'Fried Fish Sandwich', '19.00', '41.00', '423.00', '21.00', 'No', 'S', 'UNDER WEIGHT'),
(52, 'Fish Sandwich', '13.00', '15.00', '330.00', '21.00', 'No', 'S', 'UNDER WEIGHT'),
(53, 'Fish Curry ', '27.50', '13.30', '403.70', '26.40', 'No', 'D', 'UNDER WEIGHT'),
(54, 'Fish Tikka ', '23.90', '12.60', '193.00', '8.30', 'No', 'L', 'OVER WEIGHT'),
(55, 'Albacore Tuna Curry', '11.00', '8.00', '190.00', '14.00', 'No', 'D', 'UNDER WEIGHT'),
(56, 'Mackerel Curry ', '15.00', '6.00', '130.00', '5.00', 'No', 'D', 'OVER WEIGHT'),
(57, 'Fish Kofta', '10.00', '12.60', '36.10', '2.60', 'No', 'S', 'OVER WEIGHT'),
(58, 'Mughlai Kofta Curry', '0.00', '21.00', '159.00', '7.00', 'No', 'L', 'UNDER WEIGHT'),
(59, 'Salmon Fish Fingers', '11.10', '14.30', '160.50', '6.40', 'No', 'S', 'OVER WEIGHT'),
(60, 'Apollo Fish', '24.00', '5.00', '310.00', '6.00', 'No', 'D', 'NORMAL'),
(61, 'Chilli Fish', '48.00', '20.00', '434.00', '18.00', 'No', 'L', 'NORMAL'),
(62, 'Lemon Chilli Fish', '31.90', '12.90', '206.00', '206.00', 'No', 'D', 'UNDER WEIGHT'),
(63, 'fish cutlet ', '15.00', '28.00', '190.00', '2.50', 'No', 'S', 'OVER WEIGHT'),
(64, 'Chilli Prawn', '0.00', '0.00', '200.00', '0.00', 'No', 'D', 'UNDER WEIGHT'),
(65, 'Indian Prawn Curry', '14.40', '2.40', '101.00', '3.50', 'No', 'B', 'OVER WEIGHT'),
(66, 'Prawn Pulao ', '47.70', '49.90', '465.00', '8.30', 'No', 'B', 'NORMAL'),
(67, 'Fish Pakora ', '24.10', '9.70', '254.10', '12.80', 'No', 'B', 'NORMAL'),
(68, 'Spicy Prawn Pakoras', '32.00', '3.00', '250.00', '22.00', 'No', 'D', 'UNDER WEIGHT'),
(69, 'Prawn Biryani ', '25.50', '81.90', '656.00', '23.00', 'No', 'B', 'UNDER WEIGHT'),
(70, 'Ambur Chicken Biryani', '44.90', '125.40', '626.00', '14.10', 'No', 'B', 'UNDER WEIGHT'),
(71, 'Chicken Masala ', '18.00', '11.00', '140.00', '2.00', 'No', 'L', 'OVER WEIGHT'),
(72, 'Chicken Fried Rice', '10.90', '54.60', '355.00', '9.60', 'No', 'B', 'NORMAL'),
(73, 'Chicken kebabs tandoori', '33.00', '17.20', '287.30', '9.60', 'No', 'D', 'NORMAL'),
(74, 'Kalmi kebab ', '1.00', '2.40', '328.00', '35.00', 'No', 'L', 'UNDER WEIGHT'),
(75, 'Garlic Chicken', '11.00', '28.00', '200.00', '4.00', 'No', 'D', 'NORMAL'),
(76, 'Kolhapuri Chicken Curry', '32.30', '7.70', '428.00', '29.00', 'No', 'D', 'UNDER WEIGHT'),
(77, 'Gongura chicken Curry ', '1.80', '2.90', '24.00', '0.60', 'No', 'D', 'OVER WEIGHT'),
(78, 'Grilled Chicken Sandwich', '20.00', '50.00', '420.00', '20.00', 'No', 'S', 'OVER WEIGHT'),
(79, 'Chicken Hariyali Kebab ', '14.80', '7.70', '173.60', '8.80', 'No', 'D', 'OVER WEIGHT'),
(80, 'Chicken Kebab ', '16.70', '3.20', '90.00', '1.20', 'No', 'L', 'OVER WEIGHT'),
(81, 'Indian Butter Chicken', '37.00', '9.30', '387.30', '22.20', 'No', 'D', 'NORMAL'),
(82, 'Chicken Biryani ', '18.70', '22.20', '237.30', '7.80', 'No', 'L', 'NORMAL'),
(83, 'Hyderabad Chicken Biryani', '25.00', '70.50', '571.00', '19.00', 'No', 'L', 'UNDER WEIGHT'),
(84, 'chicken biryani pot ', '27.00', '52.00', '468.00', '18.00', 'No', 'L', 'NORMAL'),
(85, 'Szechuan Chicken Fried Rice', '15.00', '35.00', '401.00', '6.50', 'No', 'L', 'NORMAL'),
(86, 'chicken cutlets boneless', '29.00', '0.00', '140.00', '1.50', 'No', 'S', 'OVER WEIGHT'),
(87, 'Chicken French Fries ', '3.10', '39.80', '281.00', '15.30', 'No', 'S', 'NORMAL'),
(88, 'Afghan kebab ', '31.00', '27.00', '321.00', '10.00', 'No', 'D', 'NORMAL'),
(89, 'Chicken 65 ', '4.00', '2.00', '35.00', '1.00', 'No', 'L', 'OVER WEIGHT'),
(90, 'Chicken Ghee Roast ', '66.00', '8.00', '591.00', '32.00', 'No', 'B', 'UNDER WEIGHT'),
(91, 'Roasted Chicken ', '17.10', '0.00', '216.00', '15.90', 'No', 'L', 'NORMAL'),
(92, 'Lemon Chicken ', '43.00', '19.00', '450.00', '4.00', 'No', 'D', 'NORMAL'),
(93, 'Pepper Chicken ', '0.00', '0.00', '200.00', '0.00', 'No', 'D', 'NORMAL'),
(94, 'Chicken Lollipop ', '9.25', '3.50', '161.00', '8.50', 'No', 'S', 'NORMAL'),
(95, 'Fried Chicken Leg', '18.00', '1.00', '170.00', '9.70', 'No', 'D', 'OVER WEIGHT'),
(96, 'Fried Chicken Wings', '0.58', '0.02', '7.41', '0.66', 'No', 'S', 'OVER WEIGHT'),
(97, 'Chicken Keema ', '21.00', '0.00', '147.00', '7.00', 'No', 'D', 'OVER WEIGHT'),
(98, 'Chicken Kadai ', '13.80', '9.60', '180.00', '10.20', 'No', 'D', 'NORMAL'),
(99, 'Chicken Chilli ', '16.40', '13.00', '250.00', '8.30', 'No', 'L', 'NORMAL'),
(100, 'Reshmi Kebab ', '43.00', '5.00', '337.00', '15.80', 'No', 'D', 'NORMAL'),
(101, 'Chicken Shawarma ', '24.00', '50.00', '450.00', '15.00', 'No', 'D', 'UNDER WEIGHT'),
(102, 'Chicken Burger ', '0.00', '0.00', '297.00', '0.00', 'No', 'S', 'NORMAL'),
(103, 'Popcorn Chicken', '1.10', '1.40', '22.50', '1.40', 'No', 'S', 'OVER WEIGHT'),
(104, 'Kurma Chicken ', '13.90', '3.20', '233.00', '18.30', 'No', 'L', 'OVER WEIGHT'),
(105, 'Chicken Manchurian', '19.50', '17.40', '221.40', '8.10', 'No', 'S', 'NORMAL'),
(106, 'Chicken Nuggets ', '2.50', '2.60', '48.00', '3.00', 'No', 'S', 'OVER WEIGHT'),
(107, 'Chicken Pasta ', '40.00', '60.00', '580.00', '30.00', 'No', 'B', 'UNDER WEIGHT'),
(108, 'Chicken Tikka ', '9.50', '24.40', '183.00', '5.80', 'No', 'L', 'NORMAL'),
(109, 'Chicken Pulao', '26.00', '44.80', '326.00', '3.90', 'No', 'B', 'NORMAL'),
(110, 'Chicken Soup ', '20.00', '0.00', '200.00', '0.00', 'No', 'B', 'NORMAL'),
(111, 'Malabar chicken curry', '1.00', '6.00', '90.00', '7.00', 'No', 'D', 'OVER WEIGHT'),
(112, 'Biryani-mutton', '10.00', '80.00', '640.00', '22.50', 'No', 'L', 'UNDER WEIGHT'),
(113, 'Mutton Curry ', '30.70', '1.00', '292.00', '19.00', 'No', 'D', 'UNDER WEIGHT'),
(114, 'Fried Mutton ', '26.00', '0.00', '235.00', '14.00', 'No', 'L', 'UNDER WEIGHT'),
(115, 'Mutton Kabab', '10.00', '3.00', '135.00', '9.00', 'No', 'L', 'NORMAL'),
(116, 'Barbequed Mutton', '9.00', '0.00', '80.00', '5.00', 'No', 'D', 'OVER WEIGHT'),
(117, 'Mutton Bone Soup ', '0.00', '15.00', '50.00', '10.00', 'No', 'B', 'OVER WEIGHT'),
(118, 'Mutton keema ', '12.60', '34.20', '275.00', '9.80', 'No', 'L', 'UNDER WEIGHT'),
(119, 'Egg Biryani', '17.30', '20.00', '218.00', '10.10', 'No', 'L', 'UNDER WEIGHT'),
(120, 'Egg Fried Rice', '3.50', '27.90', '161.00', '3.30', 'No', 'B', 'NORMAL'),
(121, 'Boiled egg rice', '2.70', '16.80', '88.00', '1.20', 'No', 'B', 'OVER WEIGHT'),
(122, 'Egg Omelet', '14.10', '7.00', '316.00', '25.40', 'No', 'B', 'UNDER WEIGHT'),
(123, 'Egg Curry', '5.90', '7.50', '94.70', '4.70', 'No', 'D', 'NORMAL'),
(124, 'Egg noodles', '4.10', '18.00', '107.00', '1.50', 'No', 'D', 'NORMAL'),
(125, 'Egg Boiled ', '3.60', '0.20', '17.00', '0.20', 'No', 'B', 'OVER WEIGHT'),
(126, 'Chilli Egg Noodles', '6.50', '37.10', '198.00', '2.60', 'No', 'D', 'NORMAL'),
(127, 'Egg Masala ', '9.00', '53.00', '361.00', '13.00', 'No', 'L', 'NORMAL'),
(128, 'Scramble Egg', '6.00', '1.00', '91.00', '7.00', 'No', 'B', 'OVER WEIGHT'),
(129, 'Egg fry ', '6.00', '0.00', '70.00', '4.50', 'No', 'B', 'OVER WEIGHT'),
(130, 'Egg Keema', '12.30', '13.70', '150.00', '5.40', 'No', 'L', 'NORMAL'),
(131, 'Parota(maida)', '21.00', '50.00', '790.00', '13.00', 'No', 'B', 'UNDER WEIGHT'),
(132, 'Roti(ragi)', '6.00', '72.00', '320.00', '2.00', 'No', 'B', 'UNDER WEIGHT');

-- --------------------------------------------------------

--
-- Table structure for table `my_diet`
--

CREATE TABLE `my_diet` (
  `mydiet_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `breakfast` varchar(256) NOT NULL,
  `lunch` varchar(256) NOT NULL,
  `snacks` varchar(256) NOT NULL,
  `dinner` varchar(256) NOT NULL,
  `diet_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `diet_updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type` varchar(32) NOT NULL,
  `bf` int(11) DEFAULT NULL,
  `l` int(11) DEFAULT NULL,
  `s` int(11) DEFAULT NULL,
  `d` int(11) DEFAULT NULL,
  `bmi_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `my_diet`
--

INSERT INTO `my_diet` (`mydiet_id`, `user_id`, `breakfast`, `lunch`, `snacks`, `dinner`, `diet_created`, `diet_updated`, `type`, `bf`, `l`, `s`, `d`, `bmi_id`) VALUES
(1, 2, '08:00', '13:00', '18:00', '21:00', '2018-03-19 00:42:59', '2018-03-19 19:05:58', 'veg', 33, 7, 7, 24, 14);

-- --------------------------------------------------------

--
-- Table structure for table `my_diet_history`
--

CREATE TABLE `my_diet_history` (
  `dh_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `bf` int(11) NOT NULL,
  `l` int(11) NOT NULL,
  `s` int(11) NOT NULL,
  `d` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `dh_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `my_diet_history`
--

INSERT INTO `my_diet_history` (`dh_id`, `user_id`, `bf`, `l`, `s`, `d`, `type`, `dh_created`) VALUES
(9, 2, 33, 47, 47, 18, 0, '2018-03-19 15:37:34'),
(10, 2, 121, 50, 103, 97, 0, '2018-03-19 15:47:30'),
(11, 2, 121, 50, 103, 97, 0, '2018-03-19 15:47:30'),
(12, 2, 3, 47, 35, 18, 0, '2018-03-19 15:48:26'),
(13, 2, 121, 89, 57, 56, 0, '2018-03-19 15:49:16'),
(14, 2, 3, 47, 35, 18, 0, '2018-03-19 15:49:59'),
(15, 2, 3, 47, 35, 18, 0, '2018-03-19 15:50:34'),
(16, 2, 121, 89, 57, 56, 0, '2018-03-19 15:50:41'),
(17, 2, 129, 71, 57, 111, 0, '2018-03-19 16:03:07'),
(18, 2, 48, 89, 59, 111, 0, '2018-03-19 16:04:17'),
(19, 2, 33, 7, 7, 24, 0, '2018-03-19 16:04:41');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(256) NOT NULL,
  `username` varchar(256) NOT NULL,
  `dob` varchar(256) NOT NULL,
  `gender` varchar(16) NOT NULL,
  `mobile` varchar(16) NOT NULL,
  `password` varchar(256) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `username`, `dob`, `gender`, `mobile`, `password`, `created_on`, `updated_on`) VALUES
(1, 'babumhndr@gmail.com', 'babu', '2018-03-16', 'Male', '3214789654', '12321', '2018-03-16 14:14:53', '2018-03-16 14:14:53'),
(2, 'bob@gmail.com', 'bob', '2018-03-06', 'Male', '9874563218', '12321', '2018-03-19 00:42:59', '2018-03-19 00:42:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bmi_score`
--
ALTER TABLE `bmi_score`
  ADD PRIMARY KEY (`bmi_id`);

--
-- Indexes for table `food_items`
--
ALTER TABLE `food_items`
  ADD PRIMARY KEY (`food_id`);

--
-- Indexes for table `my_diet`
--
ALTER TABLE `my_diet`
  ADD PRIMARY KEY (`mydiet_id`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `my_diet_history`
--
ALTER TABLE `my_diet_history`
  ADD PRIMARY KEY (`dh_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bmi_score`
--
ALTER TABLE `bmi_score`
  MODIFY `bmi_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `food_items`
--
ALTER TABLE `food_items`
  MODIFY `food_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;

--
-- AUTO_INCREMENT for table `my_diet`
--
ALTER TABLE `my_diet`
  MODIFY `mydiet_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `my_diet_history`
--
ALTER TABLE `my_diet_history`
  MODIFY `dh_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
